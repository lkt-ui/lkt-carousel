import { Carousel as h, Navigation as y, Pagination as v, Slide as w } from "vue3-carousel";
import { getSlots as N } from "lkt-tools";
import { resolveComponent as n, openBlock as o, createElementBlock as p, createVNode as b, withCtx as r, createBlock as i, createCommentVNode as m, Fragment as S, renderList as B, createElementVNode as C, renderSlot as x } from "vue";
const A = {
  name: "LktCarousel",
  components: { Carousel: h, Navigation: y, Pagination: v, Slide: w },
  props: {
    showNavigation: { type: Boolean, default: !1 },
    showPagination: { type: Boolean, default: !1 },
    itemsToShow: { type: Number, default: 1 },
    autoplay: { type: Number, default: 0 },
    snapAlign: { type: String, default: "center" },
    breakpoints: {
      type: Object,
      default: () => ({})
    }
  },
  computed: {
    settings() {
      return {
        itemsToShow: this.itemsToShow,
        snapAlign: this.snapAlign
      };
    },
    slides() {
      return N(this.$slots, "slide-");
    }
  }
}, L = (e, a) => {
  const t = e.__vccOpts || e;
  for (const [l, c] of a)
    t[l] = c;
  return t;
}, P = { "data-lkt": "carousel" }, T = { "data-lkt": "slide" };
function V(e, a, t, l, c, u) {
  const _ = n("slide"), g = n("navigation"), f = n("pagination"), k = n("carousel");
  return o(), p("div", P, [
    b(k, {
      settings: u.settings,
      breakpoints: t.breakpoints,
      autoplay: t.autoplay,
      "wrap-around": !0
    }, {
      addons: r(({ slidesCount: s }) => [
        t.showNavigation && s > 1 ? (o(), i(g, { key: 0 })) : m("", !0),
        t.showPagination && s > 1 ? (o(), i(f, { key: 1 })) : m("", !0)
      ]),
      default: r(() => [
        (o(!0), p(S, null, B(u.slides, (s, d) => (o(), i(_, { key: d }, {
          default: r(() => [
            C("div", T, [
              x(e.$slots, "slide-" + d)
            ])
          ]),
          _: 2
        }, 1024))), 128))
      ]),
      _: 3
    }, 8, ["settings", "breakpoints", "autoplay"])
  ]);
}
const E = /* @__PURE__ */ L(A, [["render", V]]), q = {
  install: (e, a) => {
    e.component("lkt-carousel", E);
  }
};
export {
  q as default
};
