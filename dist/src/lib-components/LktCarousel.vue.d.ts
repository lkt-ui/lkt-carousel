import { ILktObject } from "lkt-tools";
declare const _default: {
    name: string;
    components: {
        Carousel: import("vue").DefineComponent<{
            itemsToShow: {
                default: number;
                type: NumberConstructor;
            };
            itemsToScroll: {
                default: number;
                type: NumberConstructor;
            };
            wrapAround: {
                default: boolean;
                type: BooleanConstructor;
            };
            snapAlign: {
                default: "center" | "end" | "start" | "center-even" | "center-odd";
                validator(value: string): boolean;
            };
            transition: {
                default: number;
                type: NumberConstructor;
            };
            breakpoints: {
                default: any;
                type: ObjectConstructor;
            };
            autoplay: {
                default: number;
                type: NumberConstructor;
            };
            pauseAutoplayOnHover: {
                default: boolean;
                type: BooleanConstructor;
            };
            modelValue: {
                default: undefined;
                type: NumberConstructor;
            };
            mouseDrag: {
                default: boolean;
                type: BooleanConstructor;
            };
            touchDrag: {
                default: boolean;
                type: BooleanConstructor;
            };
            dir: {
                default: "ltr" | "rtl";
                validator(value: string): boolean;
            };
            settings: {
                default(): {};
                type: ObjectConstructor;
            };
        }, () => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, import("vue").EmitsOptions, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
            itemsToShow?: unknown;
            itemsToScroll?: unknown;
            wrapAround?: unknown;
            snapAlign?: unknown;
            transition?: unknown;
            breakpoints?: unknown;
            autoplay?: unknown;
            pauseAutoplayOnHover?: unknown;
            modelValue?: unknown;
            mouseDrag?: unknown;
            touchDrag?: unknown;
            dir?: unknown;
            settings?: unknown;
        } & {
            itemsToShow: number;
            itemsToScroll: number;
            wrapAround: boolean;
            snapAlign: "center" | "end" | "start" | "center-even" | "center-odd";
            transition: number;
            breakpoints: Record<string, any>;
            autoplay: number;
            pauseAutoplayOnHover: boolean;
            mouseDrag: boolean;
            touchDrag: boolean;
            dir: "ltr" | "rtl";
            settings: Record<string, any>;
        } & {
            modelValue?: number;
        }>, {
            itemsToShow: number;
            itemsToScroll: number;
            wrapAround: boolean;
            snapAlign: "center" | "end" | "start" | "center-even" | "center-odd";
            transition: number;
            breakpoints: Record<string, any>;
            autoplay: number;
            pauseAutoplayOnHover: boolean;
            modelValue: number;
            mouseDrag: boolean;
            touchDrag: boolean;
            dir: "ltr" | "rtl";
            settings: Record<string, any>;
        }>;
        Navigation: (props: any, { slots, attrs }: any) => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>[];
        Pagination: () => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>;
        Slide: import("vue").DefineComponent<{
            index: {
                type: NumberConstructor;
                default: number;
            };
        }, () => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, import("vue").EmitsOptions, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
            index?: unknown;
        } & {
            index: number;
        }>, {
            index: number;
        }>;
    };
    props: {
        showNavigation: {
            type: BooleanConstructor;
            default: boolean;
        };
        showPagination: {
            type: BooleanConstructor;
            default: boolean;
        };
        itemsToShow: {
            type: NumberConstructor;
            default: number;
        };
        autoplay: {
            type: NumberConstructor;
            default: number;
        };
        snapAlign: {
            type: StringConstructor;
            default: string;
        };
        breakpoints: {
            type: ObjectConstructor;
            default: () => ILktObject;
        };
    };
    computed: {
        settings(): ILktObject;
        slides(): {};
    };
};
export default _default;
