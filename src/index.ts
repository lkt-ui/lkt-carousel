/* eslint-disable import/prefer-default-export */
import {default as carousel} from "./lib-components/LktCarousel.vue";
import {App} from "vue";

const LktCarousel = {
    install: (app: App, options: any) => {
        app.component('lkt-carousel', carousel);
    },
};

export default LktCarousel;